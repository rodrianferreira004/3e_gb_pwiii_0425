import { NgModule } from '@angular/core';
import { RouterModule, Routes } from '@angular/router';
import { CadImobiliariaComponent } from '../cad-imobiliaria/cad-imobiliaria.component';
import { CadImovelComponent } from './cad-imovel.component';

const routes: Routes = [
  {
    path: "",
    component: CadImovelComponent
  }
];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule]
})
export class CadImovelRoutingModule { }
